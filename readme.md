**Requires Unity 2017.1.0 or higher.**
3D Support Come Out! 


Q Inventory System is a useful and flexible inventory system which is mainly designed for 2D games. 

[Demo](https://www.youtube.com/watch?v=ctKk2oERplY) | [Tutorial](https://www.youtube.com/channel/UCCpifMR6O_DC--pZ4QxKOlQ/videos) 

It includes : 

 - Inventory 
 - Crafting
 - Equipment
 - Vendor
 - Container
 - Skillbar
 - loot

You can easily manage items, blueprints, currencies, attributes and other inventory things with a quite convenient editor extension. 

If you meet any problem or have any suggestion,leave the comments below , visit my twitter or contact me through the email in ReadMe 
(Chinese USERGUIDE and Tutorial Supported)