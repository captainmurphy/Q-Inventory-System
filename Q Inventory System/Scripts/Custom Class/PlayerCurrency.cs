﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HawkQuan
{
    [System.Serializable]
    public class PlayerCurrency
    {
        public Currency currency;
        public float amount;
        public Text showText;
    }
}

