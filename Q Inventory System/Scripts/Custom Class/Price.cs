﻿using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class Price
    {
        public Currency currency;
        public float amount;
    }
}

