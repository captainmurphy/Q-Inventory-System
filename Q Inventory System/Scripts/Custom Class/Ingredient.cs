﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class Ingredient
    {
        public Item ingredient;
        public int amount;
    }
}

