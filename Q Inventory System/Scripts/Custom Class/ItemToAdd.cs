﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class ItemToAdd
    {
        public Item itemsToAdd;
        public int amount;
        public float chance = 1;
    }
}

