﻿using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class InventoryUIControl
    {
        public GameObject m_InventoryUI;
        public KeyCode m_KeyCode = KeyCode.None;
    }
}

