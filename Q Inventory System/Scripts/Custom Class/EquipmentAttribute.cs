﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class EquipmentAttribute
    {
        public ItemAttribute equipmentItemAttribute;
        public Effect effectType;
        public float value;
    }
}

