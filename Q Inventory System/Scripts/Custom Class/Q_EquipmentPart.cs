﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class Q_EquipmentPart
    {
        public EquipmentPart equipmentPart;
        public Transform playerPart;
    }
}

