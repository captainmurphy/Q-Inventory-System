﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class PlayerAttribute
    {
        public ItemAttribute playerAttribute;
        public float currentValue = 100f;
        public float minValue = 0f;
        public float maxValue = 100f;
    }
}

