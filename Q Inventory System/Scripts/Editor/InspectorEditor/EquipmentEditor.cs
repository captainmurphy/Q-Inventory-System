﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace HawkQuan
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(EquipmentInventory))]
    public class EquipmentEditor : Editor {
        public override void OnInspectorGUI()
        {

        }
    }
}

