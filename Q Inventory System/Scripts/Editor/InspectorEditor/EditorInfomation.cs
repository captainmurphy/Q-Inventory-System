﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class EditorInfomation : ScriptableObject {
        public Color titleColor = Color.white;
        public Color labelColor = Color.white;
        public bool createRigidbody2D = true;
        public bool AutoChangeSprite = true;
    }
}

