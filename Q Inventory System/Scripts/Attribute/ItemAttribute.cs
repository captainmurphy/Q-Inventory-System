﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HawkQuan
{
    [System.Serializable]
    public class ItemAttribute : ScriptableObject{
        public string attributeName;
        public Sprite icon;
    }
}


